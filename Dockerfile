# Partie build
FROM node:18-alpine AS build

WORKDIR /app/build

COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
RUN ls /app/build  # Optional: Check the contents of the /app/build directory

# Partie serve
FROM nginx:1.23-alpine

COPY --from=build /app/build /usr/share/nginx/html

COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]


# #partie build
# FROM node:18-alpine AS build

# WORKDIR /app

# COPY package*.json ./
# RUN npm install

# COPY . .

# RUN npm run build

# #partie serve
# FROM nginx:1.23-alpine

# COPY --from=build /app/build /usr/share/nginx/html

# COPY nginx.conf /etc/nginx/conf.d/default.conf

# EXPOSE 80

# CMD [ "nginx", "-g", "daemon off;" ]